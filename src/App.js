import { useState } from 'react';
import './App.css';
import axios from 'axios';
import Swal from 'sweetalert2';


function App() {

  const [data, setdata] = useState({})
  const [location, setlocation] = useState("");
  // DEFIENE THE API WEATHER KEY
   const url = `https://api.openweathermap.org/data/2.5/weather?q=${location}&units=imperial&appid=ec17abcb6759df69354520f21af8ac59`;


   const showeAlert = () =>{
    Swal.fire({
      title:location,
      text: "The location is wrong ",
      icon:"error",
      confirmButtonText:"OK"
    }).then((result) =>{
      if (result.isConfirmed) {
        console.log("asdasdasdasdas");
      }
    })
  }



  // DEFINE THE FUCTION SEACRH LOCATION 
  const searchLocation = async(event) =>{
    if (event.key ==="Enter") {
      try {
        const locationData = await axios.get(url)
        setdata(locationData.data)
        
        setlocation(" ")
      } catch (error) { 
        showeAlert()
      }
      
    }

  }

  return (
    <div className="app">
      <div  className="search">
        <input type="text" onKeyPress={searchLocation} onChange={(e)=>  setlocation(e.target.value)} value={location} placeholder="Please Eneter location"/>
    
      </div>
        <div className="container">
            <div className="top">
                <div className="location">
                {data.name ? <p>{data.name}</p> : <p>Search the city</p> }
                </div>
                <div className="temp">
                  {data.main ? <h1>
                    {data.main.temp.toFixed()}°F</h1> : 
                    <h2>Search the location</h2>
                }
                </div>
                <div className='description'>
                    {data.weather ? <p>{data.weather[0].main}</p> : null }
                </div>
            </div>

            <div className="bottom">
                  <div className="feels">
                    { data.main ?  <p className='bold'>{data.main.feels_like.toFixed()} °F</p>: <p>Searching......</p>}
                    <p>Feels like</p>
                  </div>
                  <div className="humidity">
                  {data.main ? <p>{data.main.humidity}%</p> :<p>Searching......</p> }
                    <p>Humidity</p>
                </div>
                 <div className="wind">
                 {data.wind ? <p>{data.wind.speed}MPH</p> :<p>Searching......</p> }
                      <p>Wind Speed</p>
                    </div>
            </div>
        </div>

    </div>
  );
}

export default App;
